## OCR助手
#  :fa-github-alt: OCR助手-免费OCR工具
### :fa-flag:   **$\color{#FF3030}{基础功能全部免费，包括OCR！！！}$** 

## :fa-hand-o-right: 功能：
1.  **截图** ：支持自由框选截图，活动窗口截图，活动显示器截图，全屏截图，固定区域截图，滚动截图等
- 支持活动窗口嗅探
- 支持窗口元素嗅探，如单按钮，单图标，块布局等
2.  **贴图** ：支持文字贴图，图片贴图，粘贴板贴图等
- 文字贴图，支持 **HTML，RTF，TXT** 贴图，高保真还原文档格式
- 图片贴图，支持粘贴板贴图，截图贴图
3.  **OCR** ：支持截图识别，文件识别，表格识别，公式识别，竖排识别，PDF/Word识别，识别搜索，固定区域识别
4.  **翻译** ：OCR翻译，图片翻译，文档翻译，粘贴翻译，文字翻译，划词翻译，固定区域翻译等
5.  **工具** ：标尺，调色板，取色器等

##  :fa-gamepad: 演示
![截图功能](https://images.gitee.com/uploads/images/2021/0415/150230_f177b7ab_520205.png "截图菜单")

![OCR功能](https://images.gitee.com/uploads/images/2021/0415/152254_71e7f210_520205.png "OCR功能")

![常用工具](https://images.gitee.com/uploads/images/2021/0415/150839_1a626ee8_520205.png "常用工具合集")

![OCR演示](https://gitee.com/smartoldfish/ocr-assistant/raw/master/demo.gif "OCR演示")

## :fa-flash: 下载地址

- [ **下载地址-Gitee** ](http://cdn.oldfish.cn/update/Setup.exe?t=20230506)

- [ **下载地址-官网** ](http://cdn.oldfish.cn/update/Setup.exe?t=20230506)

注:程序本身不包含任何恶意代码，如有360等杀软误报请自行加入白名单（过卡巴，小红帽，火绒人工检测，如果报毒，请自行判断）

 
如果无法运行，请检查是否安装[.NET Framework 4.0](http://www.microsoft.com/zh-cn/download/details.aspx?id=17718)  